﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class PlayerHealth : NetworkBehaviour 
{
        [SerializeField] private int maxHp = 3;

        private Player player;

        private int currentHp;

        private void Awake() 
        {
            player = GetComponent<Player>();
        }

        [ServerCallback]
        private void OnEnable() 
        {
            currentHp = maxHp;         
           
        }

        [Server]
        public bool TakeDamage()
        {
            bool isDead = false;

            if(currentHp <= 0)
            {
               return true;
            }

            currentHp --;

            isDead = currentHp <= 0;

            RpcTakeDamage(isDead);
            return isDead;

            method = RpcTakeDamage;
            method += test;
            method(true);
        }
        [ClientRpc]
        private void RpcTakeDamage(bool dead)
        {
            if(dead)
            {
                player.Die();
            }

            
        }

        private void test(bool dfgdfg)
        {
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, transform.position, 0.5f);            
        }
	

    public delegate void DoSamething(bool isgfdgdfgd);
    private delegate int DoInt(float lll);

    public DoSamething method;
    private DoInt dfdfdfg;
    
}
