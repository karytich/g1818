﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
[Serializable]
public class ToggleEvent : UnityEvent<bool> { }
public class Player : NetworkBehaviour
{
    GameObject mainCamera;
    public ToggleEvent localEvent;
    public ToggleEvent remoteEvent;
    public ToggleEvent shareEvent;
  
    private void Start ()
    {
        mainCamera = Camera.main.gameObject;
        EnablePlayer ();
    }
    private void EnablePlayer ()
    {
       // transform.DoScaleTransform(5);
        if (isLocalPlayer)
            mainCamera.SetActive (false);
        shareEvent.Invoke (true);
        if (isLocalPlayer)
        {
            localEvent.Invoke (true);
        }
        else
        {
            remoteEvent.Invoke (true);
        }
    }
    private void DisablePlayer ()
    {
        if (isLocalPlayer)
            mainCamera.SetActive (true);
        shareEvent.Invoke (false);
        if (isLocalPlayer)
        {
            localEvent.Invoke (false);
        }
        else
        {
            remoteEvent.Invoke (false);
        }
    }
    // private void CallEvens (bool value)
    // {
    //     shareEvent.Invoke (value);
    //     if (isLocalPlayer)
    //     {
    //         localEvent.Invoke (value);
    //     }
    //     else
    //     {
    //         remoteEvent.Invoke (value);
    //     }
    // }
    public void Die ()
    {
        DisablePlayer ();
    }
}