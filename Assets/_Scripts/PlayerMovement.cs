﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerMovement : NetworkBehaviour {

	private Text text;
	private NetworkAnimator animator;
	private float speed;
	private float strafe;
	void Start () {
		animator = GetComponent<NetworkAnimator>();	
		
		//FindObjectOfType<DelegateTest>().myEvent += TestEvent;		
	}

	// private string TestEvent(int amount, out string text)
	// {
	// 	return string.Empty;
	// }
	
	private void test()
	{
		string mes = "";
		FindObjectOfType<DelegateTest>().OnClickMethod(ref mes);
		Debug.Log(mes);
	}
	
	void Update () 
	{
		if (!isLocalPlayer)
            return;


		speed = Input.GetAxis("Vertical");
		strafe = Input.GetAxis("Horizontal");
		Move(speed,strafe);
	}

	private void Move(float speed, float strafe)
	{
		animator.animator.SetFloat("Speed", speed);
		animator.animator.SetFloat("Strafe", strafe);
	}
}
