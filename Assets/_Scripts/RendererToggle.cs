﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RendererToggle : MonoBehaviour 
{

    [SerializeField] private float turnOnDelay = 0.1f;
    [SerializeField] private float turnOffDelay = 1f;
    [SerializeField] private bool enableOnLoad = false;

    private Renderer[] renderers;

    private void Awake() {
        renderers = GetComponentsInChildren<Renderer>(true);

        if(enableOnLoad)
        {
            EnableRenderer();
        }
        else
        {
            DisableRenderer();
        }
    }

    private void EnableRenderer()
    {
        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].enabled = true;
        }
    }

    private void DisableRenderer()
    {
        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].enabled = false;
        }
    }

    public void ChangeColor(Color color)
    {
        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material.color = color;
        }
    }

    public void ToggleRendererDelayer(bool isOn)
    {
        if(isOn)
        {
            Invoke("EnableRenderer", turnOnDelay);
        }
        else
        {
            Invoke("DisableRenderer", turnOffDelay);                  
        }
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.A))
        {
            StartCoroutine(WaitForSomeTime());
        }
    }

    private IEnumerator WaitForSomeTime()
    {
        for (int i = 0; i < 100; i++)
        {
            print(i);
            yield return StartCoroutine(AnyCor());

        }
        
    }

    private IEnumerator AnyCor()
    {
        yield return new WaitForSeconds(1f);
    }



	
}
