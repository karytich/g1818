﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScrollViewController : MonoBehaviour
{
	[SerializeField] private Transform content;
	[SerializeField] private Text prefab;
	[SerializeField] private Scrollbar scrollbar;
	[SerializeField] private int prefabCounter;
	[SerializeField] private float step = 0.01f;
	private IEnumerator Start ()
	{			
		Application.targetFrameRate = 30;
		for (int i = 0; i < prefabCounter; i++)
		{			
			Instantiate (prefab, content).GetComponent<Text> ().text = i.ToString ();
		}
		yield return new WaitForSeconds (1);
		while (scrollbar.value >0)
		{
			scrollbar.value -= step;
			yield return null;
		}
	}
}