﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Health : NetworkBehaviour
{
    [SerializeField]
    private int m_maxHealth = 100;
    [SyncVar (hook = "OnChangeHealth")]
    public int m_currentHealth;
    private Vector3 m_startPosition;
    [SerializeField]
    private RectTransform m_healthBar;
    [SerializeField]
    private bool destroyOnDeath;
    private void Start ()
    {
        m_startPosition = transform.position;
        m_currentHealth = m_maxHealth;
    }
    public void TakeDamage (int amount)
    {
        if (!isServer)
            return;
        m_currentHealth -= amount;
        if (m_currentHealth <= 0)
        {
             if (destroyOnDeath)
            {
                Destroy(gameObject);
            } 
            else
            {
            m_currentHealth = m_maxHealth;
            RpcRespawn ();
            }
        }
    }
    private void OnChangeHealth (int amount)
    {
        m_healthBar.sizeDelta = new Vector2 (amount / 100f, m_healthBar.sizeDelta.y);
    }
    [ClientRpc]
    private void RpcRespawn ()
    {
        if (isLocalPlayer)
        {
            transform.position = m_startPosition;
        }
    }
}