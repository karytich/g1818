﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[Serializable]
public class PlayerInfo
{
	public int Age;
	public bool IsMale;
}
[Serializable]
public class TestSet : IEnumerable<int>
{
	public IEnumerator<int> GetEnumerator ()
	{
		for (int i = 1; i <= 100; i++)
		{			
			yield return i;
		}
	}
	IEnumerator IEnumerable.GetEnumerator ()
	{
		return GetEnumerator ();
	}
}
public delegate int Delegate2 (string amount, float amount1);
public class TestScript : MonoBehaviour
{
	public Action<int> myDelegate = new Action<int> (gsagasgadgss =>
	{
		Debug.Log (gsagasgadgss);
		gsagasgadgss++;
		Debug.Log (gsagasgadgss);
	});

	public Action act = new Action(() =>{} );
	public Func<string, float, int> myFunc;
	public List<PlayerInfo> players = new List<PlayerInfo> ();
	private Delegate2 del;
	[SerializeField] private BaseClass classBase;
	ChildClass cl = new ChildClass ();
	private void Do ()
	{
		
	}
	private void Do1 (ISales sales)
	{
		sales.GetCost ();
	}
	
	private void DoInt (int amount)
	{
	}
	private int DoSome (string text, float amount)
	{
		return 1;
	}
	private void Start ()
	{
		TestSet test = new TestSet ();
		myFunc = DoSome;
		del = DoSome;
		var playerNew = test.Where (p => p % 3 == 0).Select (player => player.ToString ()).Where (pl => pl.Length < 2).ToList ().Max ();
		
		int tempMax = int.MaxValue;
		foreach (int value in new List<int> ())
		{
			if (tempMax > value)
			{
				tempMax = value;
			}
		}
		Debug.Log (tempMax);
	}
	//cl.DoSomething();
}
public interface ISales
{
	void GetCost ();
}
public interface ICost
{
	void GetPrice ();
}
public interface IClass
{
	void GetInfo ();
}
public abstract class BaseClass
{
	public void DoSomething1 (int amount)
	{
		
	}
	public abstract void DoSomethingCool ();
	protected virtual void DoSomething () { }
}
public class ChildClass : BaseClass, IClass, ISales, ICost
{
	public void DoScale () { }
	public override void DoSomethingCool () { }
	public void GetCost () { }
	public void GetInfo () { }
	public void GetPrice () { }
	protected override void DoSomething () { }
}
public class DoubleChildClass : ChildClass { }
//public delegate void Delegate2 (int amount);
public delegate void Delegate3 (string text);