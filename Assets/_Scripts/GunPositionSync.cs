﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class GunPositionSync : NetworkBehaviour
{
	[SerializeField] private Transform RightHandHold;
	[SerializeField] private Transform LeftHandHold;
	[SerializeField] private Transform cameraTransform;
	[SerializeField] private Transform handMount;
	[SerializeField] private Transform gunPivot;
	[SerializeField] private float threshold = 10f;
	[SerializeField] private float smoothing = 5f;
	[SyncVar] private float pitch;
	private Vector3 lastOffset;
	private float lastPitch = 0f;
	Animator animator;
	private void Start ()
	{
		animator = GetComponent<Animator> ();
		if (isLocalPlayer)
		{
			gunPivot.parent = cameraTransform;
		}
		else
		{
			lastOffset = handMount.position - transform.position;
		}
	}
	private void Update ()
	{
		if (isLocalPlayer)
		{
			pitch = cameraTransform.localRotation.eulerAngles.x;
			if (Mathf.Abs (lastPitch - pitch) >= threshold)
			{
				CmdUpdatePitch (pitch);
				lastPitch = pitch;
			}
		}
		else
		{
			Quaternion newRotation = Quaternion.Euler (pitch, 0, 0);
			Vector3 currentOffset = handMount.position - transform.position;
			gunPivot.localPosition += currentOffset - lastOffset;
			lastOffset = currentOffset;
			gunPivot.localRotation = Quaternion.Lerp (gunPivot.localRotation, newRotation, Time.deltaTime * smoothing);
		}
	}
	[Command]
	private void CmdUpdatePitch (float pitch)
	{
		this.pitch = pitch;
	}
	private void OnAnimatorIK (int layerIndex)
	{
		if (!animator)
			return;
		animator.SetIKPositionWeight (AvatarIKGoal.RightHand, 1f);
		animator.SetIKRotationWeight (AvatarIKGoal.RightHand, 1f);
		animator.SetIKPosition (AvatarIKGoal.RightHand, RightHandHold.position);
		animator.SetIKRotation (AvatarIKGoal.RightHand, RightHandHold.rotation);

		animator.SetIKPositionWeight (AvatarIKGoal.LeftHand, 1f);
		animator.SetIKRotationWeight (AvatarIKGoal.LeftHand, 1f);
		animator.SetIKPosition (AvatarIKGoal.LeftHand, LeftHandHold.position);
		animator.SetIKRotation (AvatarIKGoal.LeftHand, LeftHandHold.rotation);
	}
}