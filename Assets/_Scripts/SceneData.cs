﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "CreateData")]
public class SceneData:ScriptableObject
{
    public string Name;
	public int Age;
	public Color MyColor;
	
}
