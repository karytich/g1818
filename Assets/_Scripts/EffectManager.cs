﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectManager : MonoBehaviour 
{
    [SerializeField] GameObject muzzleFlashPrefab;
    [SerializeField] GameObject bulletHolePrefab;

    private ParticleSystem impactEffect;
    private ParticleSystem muzzleFlash;



    public void Init()
    {
        impactEffect = Instantiate(bulletHolePrefab).GetComponent<ParticleSystem>();
        muzzleFlash = Instantiate(muzzleFlashPrefab).GetComponent<ParticleSystem>();
    }

    public void Shot(Vector3 pos)
    {
        muzzleFlash.transform.position = pos;
        muzzleFlash.Stop();
        muzzleFlash.Play();        
    }

    public void PlayImpactEffect(Vector3 pos)
    {
        impactEffect.transform.position = pos;
        impactEffect.Stop();
        impactEffect.Play();
    }
	
}
