﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class PlayerShooting : NetworkBehaviour
{
    [SerializeField] private float ShotCooldown = 0.3f;
    [SerializeField] private Transform ShotPosition;
    [SerializeField] private EffectManager EffectManager;
    private bool canShot;
    private float timeFromShot;
    private void Start ()
    {
        EffectManager.Init ();
        if (isLocalPlayer)
        {
            canShot = true;
        }
    }
    private void Update ()
    {
        if (!canShot)
            return;
        timeFromShot += Time.deltaTime;
        if (Input.GetMouseButtonDown(0) && ShotCooldown < timeFromShot)
        {
            timeFromShot = 0;
            CmdShot(ShotPosition.position, ShotPosition.forward);
        }
    }
    [Command]
    private void CmdShot (Vector3 origin, Vector3 direction)
    {
        RaycastHit hit;
        Ray ray = new Ray (origin, direction);
        bool resual = Physics.Raycast (ray, out hit, 50f);
        if (resual)
        {
            PlayerHealth health = hit.transform.GetComponent<PlayerHealth> ();
            if (health != null)
            {
                health.TakeDamage();
                health.method(true);    
            }
        }
        RpcShotEffect (resual, hit.point);
    }
    [ClientRpc]
    private void RpcShotEffect (bool playImpact, Vector3 point)
    {
        EffectManager.Shot (ShotPosition.position);
        if (playImpact)
        {
            EffectManager.PlayImpactEffect (point);
        }
    }
}