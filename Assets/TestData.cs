﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestData : MonoBehaviour 
{
	public SceneData[] Datas;

	public UnityEngine.UI.Image image;

	private void Update() 
	{
		if(Input.GetKeyDown(KeyCode.Q))
		{
			получитьДату(0);
		}

		if(Input.GetKeyDown(KeyCode.W))
		{
			получитьДату(1);
		}

	}

	private void получитьДату(int index)
	{
		Debug.Log( Datas[index].Name );
		Debug.Log( Datas[index].Age );
		image.color = Datas[index].MyColor;
	}
}


public class WeaponeData : ScriptableObject
{
	public float damage;
	public float razbros;

	public Mesh gunMesh;

	public Texture gunTexture;
}
