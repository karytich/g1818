﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
//This script should be on player
public class PlayerController : NetworkBehaviour
{
    [SerializeField]
    private float m_speedMove;
    [SerializeField]
    private float m_speedRotate;   

    [SerializeField]
    private GameObject m_bulletPrefab;
    [SerializeField]
    private Transform m_shootPoint; 
    [SerializeField]
    private float m_bulletSpeed;
    [SerializeField]
    private float m_bulletDeathTime;
    //будет двигать игрока
    private void Update ()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        //двигать с помщью инпута
        float x = Input.GetAxis ("Horizontal") * Time.deltaTime * m_speedRotate;
        float z = Input.GetAxis ("Vertical") * Time.deltaTime * m_speedMove;
        transform.Rotate (0, x, 0);
        transform.Translate (0, 0, z);

      

        if(Input.GetKeyDown(KeyCode.Space))
        {
            CmdFire();
        }
    }

    public override void OnStartLocalPlayer()
    {       
        GetComponentInChildren<MeshRenderer>().material.color = Random.ColorHSV();
    }

    [Command]
    private void CmdFire()
    {
        GameObject bullet = Instantiate(m_bulletPrefab, m_shootPoint.position, m_shootPoint.rotation);
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * m_bulletSpeed;
        Destroy(bullet, m_bulletDeathTime);

        NetworkServer.Spawn(bullet);
    }
}