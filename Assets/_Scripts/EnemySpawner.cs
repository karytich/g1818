﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnemySpawner : NetworkBehaviour 
{
    [SerializeField]
    private GameObject m_botPrefab;

    [SerializeField]
    private int m_enemyCount;

    public override void OnStartServer()
    {
        for (int i = 0; i < m_enemyCount; i++)
        {
            Vector3 spawnPositin = new Vector3(Random.Range(-4, 4), 0, Random.Range(-4, 4));
            Quaternion spawnRotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
            GameObject bot = Instantiate(m_botPrefab, spawnPositin, spawnRotation);
            NetworkServer.Spawn(bot);
        }
    }
	
}
