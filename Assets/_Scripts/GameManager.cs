﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class GameManager : MonoBehaviour
{
	[SerializeField]
	private ExcelReader excelReader;
	
	public data data;

	private void Start() 
	{
		data = excelReader.GetData("list");
	}
}