﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExcelReader : MonoBehaviour
{
	
	public data GetData(string listdata)
	{
		TextAsset list = Resources.Load<TextAsset>(listdata);
		string[] row = list.text.Split(new char[] {'\n'});
		data dataFromExcel = new data();
		for (int i = 1; i < row.Length; i++)
		{
			string[] data = row[i].Split(new char[] {','} );
			dataFromExcel.levels.Add(Int32.Parse(data[0]));
			dataFromExcel.counts.Add (Int32.Parse(data[1]));
			dataFromExcel.text .Add(data[2]);
		}
		
		return dataFromExcel;
	}
	
}

[Serializable]
public class data
{
	public List<int> levels = new List<int>();
	public List<int> counts= new List<int>();
	public List<string> text= new List<string>();
}

