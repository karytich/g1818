﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public delegate void Method();



public delegate string StringMethod(int value);
public class DelegateTest : MonoBehaviour 
{
	public Action<int> myAction = new Action<int>((x) => {});

	public Func<int, string> myFunc;

	public event StringMethod myEvent;
	public Method m_method;
	private StringMethod m_stringMethod;
	private Button m_button;
	private Text m_buttonText;

	//private string message;
	void Start () {
		// m_button = GetComponentInChildren<Button>();
		// m_buttonText = GetComponentInChildren<Text>();
		// m_button.onClick.AddListener(OnClickMethod);
		// //m_button.onClick.AddListener(StartPlay);
		
		// m_method = StartPlay;	
		///OnClickMethod(out message);
		//Debug.Log(message);
			
	}

	public void OnClickMethod(ref string message)
	{
		message = "Hello";
	}




	private void OnDestroy() {
		m_button.onClick.RemoveAllListeners();
	}
	
	 private void StartPlay()
	 {
		Debug.Log("Method 1");
		m_method = Restart;
	 }

	  private void Restart()
	 {		
		Debug.Log("Method test");
		m_method = NextLevel;
	 }

	  private void NextLevel()
	 {
		Debug.Log("Method Example");
		m_method = StartPlay;
	 }

}
