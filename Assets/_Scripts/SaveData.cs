﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
public class SaveData : MonoBehaviour
{
	[SerializeField] playerInfo playerInfo;
	private void Awake ()
	{
		Load ();
		adress ad = new adress ();
		ad.GetAdress (new string ('-', 50));
	}
	private string Name = "Player";
	private int HP = 100;
	void OnGUI ()
	{
		GUI.Label (new Rect (10, 10, 100, 50), Name + " " + HP + "HP");
		if (GUI.Button (new Rect (10, 40, 100, 20), "Save"))
		{
			Save ();
		}
		if (GUI.Button (new Rect (10, 70, 100, 20), "Load"))
		{
			Load ();
		}
		if (GUI.Button (new Rect (10, 110, 100, 20), "AddHp"))
		{
			AddHp ();
		}
		if (GUI.Button (new Rect (10, 150, 100, 20), "SubHp"))
		{
			SubHp ();
		}
	}
	public void Save ()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/playerInfo.dat");
		playerInfo player = new playerInfo ();
		player.NikeName = "Artem";
		player.CurHp = HP;
		player.IndexInGame = player.GetHashCode ();
		bf.Serialize (file, player);
		file.Close ();
	}
	public void Load ()
	{
		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat"))
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			playerInfo player = (playerInfo) bf.Deserialize (file);
			Name = player.NikeName;
			HP = player.CurHp;
			file.Close ();
		}
	}
	private void OnApplicationQuit ()
	{
		Save ();
	}
	public void AddHp ()
	{
		HP += 100;
	}
	public void SubHp ()
	{
		HP -= 5;
	}
}
[Serializable]
public class playerInfo
{
	public string NikeName;
	public int IndexInGame;
	public int CurHp;
	public float posX;
	public float posY;
	public float posZ;
}
public class adress
{
	public void GetAdress (string text)
	{
	}
}