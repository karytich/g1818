﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "AutoLevel", menuName = "CreateAutoLevel")]
public class AutoLevelGenerator : ScriptableObject 
{
	public List<RotationSetting> RotationSettings;
	public ножи[] Ножи;
}

[Serializable]
public class RotationSetting
{	
	public Difference difference;
	public float speed;
	public int ChageDirectionTime;
}

[Serializable]
public enum Difference
{
	Easy,
	Normal,
	Hard
}
[Serializable]
public class ножи
{
	public GameObject Нож;
}
