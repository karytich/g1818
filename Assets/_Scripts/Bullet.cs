﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour 
{
    [SerializeField]
    private int m_damage = 10;
   private void OnCollisionEnter(Collision other) 
   {
        var hit = other.gameObject;
        var health = hit.GetComponentInParent<Health>();

        if(health != null)
        {
            health.TakeDamage(m_damage);
        }

        Destroy(gameObject);
    }
	
}
