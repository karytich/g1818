﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UITestScript : MonoBehaviour
{
	[SerializeField] private Image m_image;
	[SerializeField] private InputField m_InputField;
	private Slider m_slider;
	private Button m_button;
	private Coroutine m_coroutine;
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.A))
		{
			if (m_coroutine == null)
				m_coroutine = StartCoroutine (ProgressBar ());
			m_button.interactable = !m_button.interactable;
		}
		if (Input.GetKeyDown (KeyCode.Q))
		{
			StopCoroutine (m_coroutine);
			m_coroutine = null;
		}


		m_InputField.text = "";
#if !UNITY_ANDROID
		m_InputField.shouldHideMobileInput = true;
		#elif UNITY_EDITOR

#endif
	}
#if UNITY_EDITOR	
	private IEnumerator ProgressBar ()
	{
		while (true)
		{
			while (m_image.fillAmount < 1)
			{
				m_image.fillAmount += Time.deltaTime;
				yield return null;
			}
			m_image.fillClockwise = false;
			while (m_image.fillAmount > 0)
			{
				m_image.fillAmount -= Time.deltaTime;
				yield return null;
			}
			m_image.fillClockwise = true;
		}
	}
#endif
}